package com.hcl.spring.jenkins;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevopsAssessmentApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(DevopsAssessmentApplication.class, args);
		System.out.println("Hello....DevOps.....");
	}
	@Override
	public void run(String... args) throws Exception {
		System.out.println("Hello....DevOps.....");
		
	}

}
